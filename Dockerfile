FROM java:openjdk-8u111-jre
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6 && \
	echo "deb http://repo.mongodb.com/apt/debian jessie/mongodb-enterprise/3.4 main" | tee /etc/apt/sources.list.d/mongodb-enterprise.list && \
	apt-get update && \
	apt-get install -y mongodb-enterprise && \
	mkdir -p /data/db && \
	rm -rf /var/lib/apt/lists/*
